﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcreteMocking
{
    public interface IInterface
    {
        public int InterfaceProperty { get; set; }

        public object InterfaceFunction();

        public void InterfaceAction();
    }
}
