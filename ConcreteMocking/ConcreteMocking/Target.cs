﻿using System;

namespace ConcreteMocking
{
    public class Target : TargetBase, IInterface
    {
        //public static string Name = "Name";
        //public const string Description = "Description";

        //instance
        public string InstanceFunction() => "hstus pan";

        public void InstanceAction(string value)
        {
            Console.WriteLine($"hstus {value} pan");
        }

        public int InstanceProperty { get; set; }

        //static
        public static string StaticFunction()
        {
            return "static hstus";
        }

        public static void StaticAction()
        {
            Console.WriteLine("static hstus");
        }

        //interface
        public object InterfaceFunction()
        {
            return "interface hstus";
        }

        public void InterfaceAction()
        {
            Console.WriteLine("interface hstus!");
        }

        public int InterfaceProperty { get; set; }

        //virtuals
        public override void OverridenVirtualAction()
        {
            Console.WriteLine("overriden hstus");
        }

        public virtual void IntroducedVirtualAction()
        {
            Console.WriteLine("introduced hstus");
        }
    }
}
