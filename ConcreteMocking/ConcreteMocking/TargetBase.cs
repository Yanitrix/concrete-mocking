﻿namespace ConcreteMocking
{
    public class TargetBase
    {
        public void InheritedAction()
        {
            System.Console.WriteLine("inherited hstus");
        }

        public virtual void OverridenVirtualAction()
        {
            System.Console.WriteLine("overridable hstus");
        }

        public virtual void NotOverridenVirtualAction()
        {
            System.Console.WriteLine("not overridable hstus");
        }
    }
}
