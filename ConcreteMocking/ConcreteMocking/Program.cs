﻿using System.Reflection;
using static ConcreteMocking.Replacer;

namespace ConcreteMocking;

public static class Program
{
    public static void Main(string[] args)
    {
        //TODO remember to replace inherited on both super and sub and see what happens
        //TODO also play with properties

        //Console.WriteLine(target.InstanceFunction());
        //target.InterfaceAction();

        var instanceFunction = Method("InstanceFunction");
        var instanceAction = Method("InstanceAction");
        var inherited = Method("InheritedAction");
        //var instanceProperty = Method("InstanceProperty"); //won't work
        var staticFunction = Method("StaticFunction");
        var staticAction = Method("StaticAction");

        var interfaceFunction = Method("InterfaceFunction");
        var interfaceAction = Method("InterfaceAction");
        //var interfaceProperty = Method("InterfaceProperty"); //also wont work
        var overridenVirtual = Method("OverridenVirtualAction");
        var notOverridenVirtual = Method("NotOverridenVirtualAction");
        var introducedVirtual = Method("IntroducedVirtualAction");

        Console.WriteLine(interfaceAction.IsVirtual);
        Console.WriteLine(interfaceAction.IsVirtual);
        //Console.WriteLine(interfaceProperty.IsVirtual);
        Console.WriteLine(overridenVirtual.IsVirtual);
        Console.WriteLine(notOverridenVirtual.IsVirtual);
        Console.WriteLine(introducedVirtual.IsVirtual);

        //closure cannot be used
        //var str = "hstus pan!";
        //var m3 = new Func<string>(() => str).GetMethodInfo();

        var functionReplacement = new Func<object>(() => "hstus replacement");
        var actionReplacement = new Action(() => Console.WriteLine("Hstus replacement"));
        var function = functionReplacement.GetMethodInfo();
        var action = actionReplacement.GetMethodInfo();

        ReplaceMethod(instanceFunction, function);
        ReplaceMethod(instanceAction, action);
        ReplaceMethod(inherited, action);
        ReplaceMethod(staticFunction, function);
        ReplaceMethod(staticAction, action);

        ReplaceVirtualMethod(interfaceFunction, function);
        ReplaceVirtualMethod(interfaceAction, action);
        ReplaceVirtualMethod(overridenVirtual, action);
        ReplaceVirtualMethod(notOverridenVirtual, action);
        ReplaceVirtualMethod(introducedVirtual, action);

        var target = new Target();

        Console.WriteLine(target.InstanceFunction());
        target.InstanceAction("hstus"); //TODO it should crash here //it didn't lol
        target.InheritedAction();
        Console.WriteLine(Target.StaticFunction());
        Target.StaticAction();

        Console.WriteLine("\nvirtuals start here\n");

        Console.WriteLine(target.InterfaceFunction());
        target.InterfaceAction();
        target.OverridenVirtualAction();
        target.NotOverridenVirtualAction();
        target.IntroducedVirtualAction();
        target.InheritedAction();

        //Console.WriteLine("\nend of debyugging");


        //Console.WriteLine("start of another test run\n");

        var superClassMethod = Method(typeof(TargetBase), "InheritedAction");
        ReplaceMethod(superClassMethod, action);

        new TargetBase().InheritedAction();



        //var result = target.InstanceFunction();
        //target.InterfaceAction();

        //Console.WriteLine(result is null);
        //Console.WriteLine(result.GetHashCode());

        //unsafe
        //{
        //    Service target = new();

        //    Console.WriteLine(target.GetString());

        //    var m1 = typeof(Service).GetMethod("GetString", BindingFlags.Instance | BindingFlags.Public);
        //    var m2 = typeof(Injection).GetMethod("injectionMethod2", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

        //    Injection.ReplaceMethod(m1, m2);

        //    Console.WriteLine(target.GetString().Length);
        //}

        //var service = FormatterServices.GetUninitializedObject(typeof(Service)) as Service;

        //var service = new Service();

        //Console.WriteLine(Service.Name);
        //Console.WriteLine(Service.Description);

        //Action action = () => Console.WriteLine(12);
        //var i = 12;
        //Action action1 = () => Console.WriteLine(i);
        //Func<int, int> action2 = (int input) => i;

        //var target = action.Target;
        //var target1 = action1.Target;

        //Console.WriteLine(action.Target.GetType());
        //Console.WriteLine(action1.Target.GetType());
        //Console.WriteLine(action2.Target.GetType());
    }

    private static MethodInfo Method(string name)
    {
        return typeof(Target).GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
    }

    private static MethodInfo Method(Type targetType, string name)
    {
        return targetType.GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
    }
}
